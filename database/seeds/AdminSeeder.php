<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\App\User::create([
    		'username' => 'admin',
    		'password' => bcrypt('admin'),
    		'level' => 0
    	]);
    }
}
