@extends('layouts.app')
@section('title', 'Category')
@section('content')
<div class="row mb-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ url('category/' . @$category->id) }}">
                    {{ csrf_field() }}
                    {{ @$category ? method_field('PATCH') : '' }}
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control p-input" placeholder="Masukkan Nama Kategori" name="nama" value="{{ old('nama', @$category->nama) }}">
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                        <input type="text" class="form-control p-input" placeholder="Masukkan Slug" name="slug" value="{{ old('slug', @$category->slug) }}">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection